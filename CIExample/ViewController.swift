//
//  ViewController.swift
//  CIExample
//
//  Created by Carlos Henrique on 17/01/2019.
//  Copyright © 2019 Carlos Henrique. All rights reserved.
//

import UIKit
import AppCenterAnalytics

class ViewController: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    
    let value = doMath(num1: 2, num2: 2)
    
    print("Two plus two is \(value) minus one, that's quick maths!!!")
    
    MSAnalytics.trackEvent("My custom event")


  }

  func doMath(num1 : Int, num2: Int) -> Int{
    return num1 + num2
  }

}

